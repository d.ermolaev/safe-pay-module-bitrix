<?php

$moduleID = "safepay.blockchain";
$landPref = "SAFEPAY_BLOCKCHAIN_";

Bitrix\Main\Loader::includeModule($moduleID);

if (!$USER->CanDoOperation('safepay.blockchain_settings')) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

$arAllOptions = array(
    array("isTest", GetMessage($landPref . 'isTest'), array("checkbox"), "Y"),
    array("publicKey", GetMessage($landPref . 'publicKey'), array("text"), ""),
    array("privateKey", GetMessage($landPref . 'privateKey'), array("text"), ""),
    array("bild", GetMessage($landPref . 'bild'), array("text"), ""),
    array("nextStep", GetMessage($landPref . 'nextStep'), array("textReadOnly"), ""),
    array("nextStepTest", GetMessage($landPref . 'nextStepTest'), array("textReadOnly"), ""),
);

$aTabs = array(
    array("DIV" => "edit1", "TAB" => GetMessage($landPref . 'TAB_NAME'), "ICON" => "", "TITLE" => GetMessage($landPref . 'TAB_NAME_TITLE')),
    array("DIV" => "edit2", "TAB" => GetMessage("MAIN_TAB_RIGHTS"), "ICON" => "", "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && check_bitrix_sessid()) {
    if (strlen($RestoreDefaults) > 0) {
        COption::RemoveOption($moduleID);
        $z = CGroup::GetList($v1 = "id", $v2 = "asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
        while ($zr = $z->Fetch()) {
            $APPLICATION->DelGroupRight($moduleID, array($zr["ID"]));
        }
    }
    if (strlen($_POST['generatekey']) > 0) {
        $cp = new SafePay\Blockchain\Cryptobase();
        $account = $cp->generateAccount();
        COption::SetOptionString($moduleID, 'publicKey', $account['publicKey']);
        COption::SetOptionString($moduleID, 'privateKey', $account['privateKey']);
    } else {
        COption::SetOptionString($moduleID, 'isTest', $_POST['isTest']);
        COption::SetOptionString($moduleID, 'publicKey', $_POST['publicKey']);
        COption::SetOptionString($moduleID, 'privateKey', $_POST['privateKey']);
        COption::SetOptionString($moduleID, 'bild', $_POST['bild']);
    }
}

$tabControl->Begin();?>
<form method="POST"  enctype="multipart/form-data" name="antispampro_settings" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($mid)?>&amp;lang=<?echo LANG?>">
	<?=bitrix_sessid_post();?>
<?
$tabControl->BeginNextTab();
?>
<?
foreach($arAllOptions as $arOption):
	$val = COption::GetOptionString($moduleID, $arOption[0], $arOption[3]);
	$type = $arOption[2];

?>
<tr>
	<td width="40%" nowrap><?
	//if ($type[0] == "checkbox")
		echo "<label for=\"".htmlspecialcharsbx($arOption[0])."\">".$arOption[1].":</label>";
	/*else
		echo $arOption[1];*/
?> </td>
	<td width="60%"><?
	if($type[0]=="checkbox"):
		?><input type="checkbox" name="<?echo htmlspecialcharsbx($arOption[0])?>" id="<?echo htmlspecialcharsbx($arOption[0])?>" value="Y"<?if($val=="Y")echo" checked";?> /><?
	elseif ($type[0]=="text"):
		?><input id="<?echo htmlspecialcharsbx($arOption[0])?>" type="text" size="<?echo $type[1]?>" maxlength="2550" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($arOption[0])?>" style="width:50%;" />
		<?
    elseif ($type[0]=="textReadOnly"):
		?><input id="<?echo htmlspecialcharsbx($arOption[0])?>" type="text" readonly="readonly" size="<?echo $type[1]?>" maxlength="2550" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($arOption[0])?>" style="width:50%;" />
		<?
	elseif($type[0]=="textarea"):
		?><textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialcharsbx($arOption[0])?>" style="width:90%;"><?echo htmlspecialcharsbx($val)?></textarea><?
	endif;
	?></td>
</tr>
<?endforeach;?>

<?
$tabControl->BeginNextTab();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
$tabControl->Buttons();?>
<input type="submit" name="Update" value="<?echo GetMessage("MAIN_SAVE")?>">
<input type="hidden" name="Update" value="Y">
<input type="submit" name="generatekey" title='<?=GetMessage($landPref . "GENERATE_KEY")?>' value='<?=GetMessage($landPref . "GENERATE_KEY")?>'>
<?if(
	COption::GetOptionString($moduleID, "publicKey", "") != ""
	&& COption::GetOptionString($moduleID, "privateKey", "") != ""
) :?>
    <script src="/bitrix/safepay.blockchain/js/Base58.js"></script>
    <script src="/bitrix/safepay.blockchain/js/eralib.js"></script>
    <script src="/bitrix/safepay.blockchain/js/ripemd160.js"></script>
    <script src="/bitrix/safepay.blockchain/js/sha256.js"></script>
	<input type="submit" name="generatebild" onclick="generateBild();" title='<?=GetMessage($landPref . "GENERATE_BILD")?>' value='<?=GetMessage($landPref . "GENERATE_BILD")?>'>
	<script>
		function generateBild() {
			var publickey = document.getElementById('publicKey').value;
			var bild = getAccountAddressFromPublicKey(publickey);
			document.getElementById('bild').value = bild;
		}
	</script>
<?endif;?>
<?
$tabControl->End();
?>
</form>
