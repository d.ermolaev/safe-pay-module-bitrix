<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Результат оплаты");?>

<p>Ваш заказ не был оплачен.</p>
<p>Если вы оплатили заказ, и в течение часа статус заказа не поменялся, пожалуйста, обратитесь в магазин.</p>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>