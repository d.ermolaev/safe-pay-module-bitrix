CREATE TABLE safe_pay_invoice (
  ID int(18) NOT NULL AUTO_INCREMENT,
  STATUS varchar(50) NOT NULL,
  RECIPIENT varchar(5) NOT NULL,
  BANK_ID varchar(150) NOT NULL,
  EXPIRE varchar(100) NOT NULL,
  PAY_NUM varchar(50) NOT NULL,
  PAYSYSTEM_ID int(18) NOT NULL,
  IS_TEST varchar(1) NOT NULL,
  PAYSYSTEM_TYPE varchar(100) NOT NULL,
  DATE_CREATED varchar(100) NOT NULL,
  DATE_UPDATED varchar(100) NOT NULL,
  CREATOR varchar(1024) NOT NULL,
  SITE_URL varchar(512) NOT NULL,
  PRIMARY KEY (ID)
);

CREATE TABLE safe_pay_recipient (
  ID int(18) NOT NULL AUTO_INCREMENT,
  NAME varchar(100) NOT NULL,
  ATRIBUTE varchar(5) NOT NULL,
  BILD varchar(100) NOT NULL,
  PUBLIC_KEY varchar(100) NOT NULL,
  PAY_URL varchar(150) NOT NULL,
  APP_ANDROID varchar(150),
  APP_IOS varchar(150),
  PICTURE_URL varchar(150) NOT NULL,
  PRIMARY KEY (ID)
);

INSERT INTO safe_pay_recipient (NAME, ATRIBUTE, BILD, PUBLIC_KEY, PAY_URL, APP_ANDROID, APP_IOS, PICTURE_URL)
  VALUES ('ТрансСтройБанк', 'tsbnk', '7Dpv5Gi8HjCBgtDN1P1niuPJQCBQ5H8Zob', '2M9WSqXrpmRzaQkxjcWKnrABabbwqjPbJktBDbPswgL7', 'https://online.transstroybank.ru/', 'com.isimplelab.ibank.tsbank', "/ru/app/тсб-онлайн/id723491575?mt=8", '/bitrix/images/safepay.blockchain/tsbnk.png');

CREATE TABLE safe_pay_server (
  ID int(18) NOT NULL AUTO_INCREMENT,
  TYPE_SERVER varchar(50) NOT NULL,
  URL_SERVER varchar(150) NOT NULL,
  TIME_UPDATE varchar(100) NOT NULL,
  PRIMARY KEY (ID)
);

INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('test','http://89.235.184.229:9067','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://138.68.232.232:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://138.68.186.214:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://138.68.225.51:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://46.101.14.242:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://138.197.135.122:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://207.154.249.81:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://207.154.214.55:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://138.197.143.167:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://206.81.0.11:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://193.42.145.120:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://204.48.28.103:9047','0');
INSERT INTO safe_pay_server (TYPE_SERVER, URL_SERVER, TIME_UPDATE) VALUES ('live','http://explorer.erachain.org:9047','0');
