<?php
IncludeModuleLangFile(__FILE__);

class safepay_blockchain extends CModule
{
    public $MODULE_ID = "safepay.blockchain";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;

    public function safepay_blockchain()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include $path . "/version.php";
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = GetMessage('SAFEPAY_BLOCKCHAIN_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('SAFEPAY_BLOCKCHAIN_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = GetMessage('SAFEPAY_BLOCKCHAIN_NAME_COMPANY');
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_URI = 'http://safe-pay.ru/';
    }

    public function DoInstall()
    {
        $isConverted = \Bitrix\Main\Config\Option::get('main', '~sale_converted_15', "N");
        if (!CModule::IncludeModule('sale') || !(defined("SM_VERSION") && version_compare(SM_VERSION, "16.0.0") >= 0 && $isConverted == "Y")) {
            return false;
        }

        if (!$this->InstallDB()) {
            return false;
        }

        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $this->MODULE_ID . "/install/admin",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);

        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/public/handler',
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/sale_payment/safepay', true, true);
        if (SITE_CHARSET == "UTF-8") {
            CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/public/page_utf8',
                $_SERVER['DOCUMENT_ROOT'] . '/bitrix/safepay.blockchain', true, true);
        } else {
            CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/public/page_win1251',
                $_SERVER['DOCUMENT_ROOT'] . '/bitrix/safepay.blockchain', true, true);
        }

        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/public/safepay.png',
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix/images/sale/sale_payments/safepay.png', true, true);

        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $this->MODULE_ID . "/images",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/images/" . $this->MODULE_ID, true, true);

        RegisterModule($this->MODULE_ID);

        CAgent::AddAgent("\SafePay\Blockchain\Agent::resultPay();", $this->MODULE_ID, "N", 300, "", "Y", "", 100);
        CAgent::AddAgent("\SafePay\Blockchain\Agent::availableServers();", $this->MODULE_ID, "N", 1500, "", "Y", "", 100);

        return true;
    }

    public function DoUninstall()
    {
        $this->UnInstallDB(array(
            "savedata" => $_REQUEST["savedata"],
        ));

        DeleteDirFilesEx("/bitrix/php_interface/include/sale_payment/safepay");
        DeleteDirFilesEx("/bitrix/safepay.blockchain");

        UnRegisterModule($this->MODULE_ID);
        CAgent::RemoveModuleAgents($this->MODULE_ID);
        return true;
    }

    public function InstallDB()
    {
        global $DB, $DBType, $APPLICATION;
        $errors = false;

        if (!$DB->Query("SELECT 'x' FROM safe_pay_invoice WHERE 1=0", true)) {
            $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $this->MODULE_ID . "/install/db/" . strtolower($DB->type) . "/install.sql");
        }

        if ($errors !== false) {
            $APPLICATION->ThrowException(implode("<br>", $errors));
            return false;
        }

        return true;
    }

    public function UnInstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;
        $errors = false;

        if (!array_key_exists("savedata", $arParams) || $arParams["savedata"] != "Y") {
            $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $this->MODULE_ID . "/install/db/" . strtolower($DB->type) . "/uninstall.sql");
        }

        if ($errors !== false) {
            $APPLICATION->ThrowException(implode("<br>", $errors));
            return false;
        }

        return true;
    }
}
