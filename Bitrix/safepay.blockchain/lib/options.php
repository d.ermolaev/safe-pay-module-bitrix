<?php
namespace SafePay\Blockchain;

use Bitrix\Main\Config\Option;

class Options
{
    const MODULE_ID = 'safepay.blockchain';

    public static function publicKey()
    {
        return Option::get(self::MODULE_ID, 'publicKey', '');
    }

    public static function privateKey()
    {
        return Option::get(self::MODULE_ID, 'privateKey', '');
    }

    public static function bild()
    {
        return Option::get(self::MODULE_ID, 'bild', '');
    }

    public static function isTest()
    {
        return Option::get(self::MODULE_ID, 'isTest', '') == 'Y';
    }

    public static function getNextStep()
    {
        $result = '';
        if (self::isTest()) {
            $result = Option::get(self::MODULE_ID, 'nextStepTest', '1');
        } else {
            $result = Option::get(self::MODULE_ID, 'nextStep', '1');
        }
        return $result;
    }

    public static function setNextStep($next)
    {
        if (self::isTest()) {
            Option::set(self::MODULE_ID, 'nextStepTest', $next);
        } else {
            Option::set(self::MODULE_ID, 'nextStep', $next);
        }
        return true;
    }
}
