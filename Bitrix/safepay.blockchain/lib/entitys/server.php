<?php
namespace SafePay\Blockchain\Entitys;

use Bitrix\Main\Entity;

class ServerTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'safe_pay_server';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new Entity\StringField('TYPE_SERVER'),
            new Entity\StringField('URL_SERVER'),
            new Entity\StringField('TIME_UPDATE'),
        );
    }
}
