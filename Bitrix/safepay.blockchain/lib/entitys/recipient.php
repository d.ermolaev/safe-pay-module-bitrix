<?php
namespace SafePay\Blockchain\Entitys;

use Bitrix\Main\Entity;

class RecipientTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'safe_pay_recipient';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new Entity\StringField('NAME'),
            new Entity\StringField('ATRIBUTE'),
            new Entity\StringField('BILD'),
            new Entity\StringField('PUBLIC_KEY'),
            new Entity\StringField('PAY_URL'),
            new Entity\StringField('APP_ANDROID'),
            new Entity\StringField('APP_IOS'),
            new Entity\StringField('PICTURE_URL'),
        );
    }

    public static function getByAtribute($attr)
    {
        return RecipientTable::getList(array(
            'filter' => array('ATRIBUTE' => $attr),
        ))->fetch();
    }

    public static function getListAll()
    {
        $result = array();

        $dbRecipient = RecipientTable::getList(array(
            'select' => array('ATRIBUTE','BILD')
        ));

        while ($item = $dbRecipient->fetch()) {
            $result[$item['ATRIBUTE']] = $item['BILD'];
        }

        return $result;
    }
}
