<?php
namespace SafePay\Blockchain\Entitys;

use Bitrix\Main\Entity;

class InvoiceTable extends Entity\DataManager
{
    const STATUS_ACTIVE = 'active';
    const STATUS_FINISH = 'finish';
    const STATUS_WAITING = 'waiting';
    const STATUS_CANCELED = 'canceled';
    const STATUS_EXPIRED = 'expired';

    public static function getTableName()
    {
        return 'safe_pay_invoice';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new Entity\StringField('STATUS'),
            new Entity\StringField('RECIPIENT'),
            new Entity\StringField('BANK_ID'),
            new Entity\StringField('EXPIRE'),
            new Entity\StringField('PAY_NUM'),
            new Entity\StringField('IS_TEST'),
            new Entity\StringField('PAYSYSTEM_ID'),
            new Entity\StringField('PAYSYSTEM_TYPE'),
            new Entity\StringField('DATE_CREATED'),
            new Entity\StringField('DATE_UPDATED'),
            new Entity\StringField('CREATOR'),
            new Entity\StringField('SITE_URL'),
        );
    }

    public static function updateStatus($id, $status)
    {
        if (intval($id) > 0) {
            InvoiceTable::Update($id, array(
                'STATUS' => $status,
                'DATE_UPDATED' => date("d.m.Y H:i:s"),
            ));
        }
        return true;
    }

    public static function isActive($paymentID)
    {
        $dbInvoice = InvoiceTable::getList(array(
            "filter" => array(
                'STATUS' => InvoiceTable::STATUS_ACTIVE,
                'PAY_NUM' => $paymentID,
            ),
        ));

        if ($arInvoice = $dbInvoice->fetch()) {
            return $arInvoice;
        }

        return false;
    }

    public static function deactiveExpiring()
    {
        $dbInvoices = InvoiceTable::getList(array(
            "filter" => array("STATUS" => self::STATUS_ACTIVE),
        ));

        while ($invoice = $dbInvoices->fetch()) {
            $timestamp = strtotime('-1 hours');
            if ($timestamp > $invoice["EXPIRE"]) {
                InvoiceTable::Update($invoice["ID"], array(
                    'STATUS' => self::STATUS_EXPIRED,
                    'DATE_UPDATED' => date("d.m.Y H:i:s"),
                ));
            }
        }
        return true;
    }
}
