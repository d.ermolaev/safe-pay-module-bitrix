<?php
namespace SafePay\Blockchain\Helpers;

use Bitrix\Main\Web\Json as BJson;
use Bitrix\Main\ArgumentException;

class Json
{
    public static function decode($data)
    {
        $result = array();
        try {
            $result = BJson::decode($data);
        } catch (ArgumentException $e) {
            $result = array();
        }
        return $result;
    }
}
