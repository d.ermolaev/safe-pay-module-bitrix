<?php
namespace SafePay\Blockchain\Helpers;

use Bitrix\Main\Application;
use Bitrix\Main\Web\Json;

class Security
{
    public static function encode($arData)
    {
        $result = array();
        $result["DATA"] = urlencode(base64_encode(gzcompress(Json::encode($arData))));
        $result["HASH"] = self::getHash($arData);
        return $result;
    }

    public static function decode($str)
    {
        $result = array();
        $result["DATA"] = (array) Json::decode(gzuncompress(base64_decode(urldecode($str))));
        $result["HASH"] = self::getHash($result["DATA"]);
        return $result;
    }

    protected function getHash($arData)
    {
        $i == 0;
        $arData1 = array();
        $arData2 = array();
        foreach ($arData as $key => $value) {
            if ($i % 2 == 0) {
                $arData1[$key] = $value;
            } else {
                $arData2[$key] = $value;
            }
        }
        return hash('sha256', hash('sha256', Json::encode($arData1)) . Json::encode($arData2));
    }
}
