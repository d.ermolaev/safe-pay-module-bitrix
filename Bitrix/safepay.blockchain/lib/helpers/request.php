<?php
namespace SafePay\Blockchain\Helpers;

use SafePay\Blockchain\Entitys\ServerTable;
use SafePay\Blockchain\Options;

class Request
{
    const STATUS_OK = 'OK';
    const STATUS_ERROR = 'ERROR';

    private $serversSafePay = array();

    public function __construct()
    {
        $this->serversSafePay = self::getAvailable();
    }

    public function send($urlPrefix, $param = false)
    {
        foreach ($this->serversSafePay as $server) {
            $url = $server["URL_SERVER"] . $urlPrefix;
            $response = self::makeCURl($url, $param);
            if ($response['STATUS'] == self::STATUS_OK) {
                return $response;
            }
        }
        return $response;
    }

    public static function sendPay($url, $param = false)
    {
        return self::makeCURl($url, $param);
    }

    private function makeCURl($url, $param = false)
    {
        $result = array();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if ($param) {
            curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($param));
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }
        $response = curl_exec($ch);

        if ($response === false) {
            $result['ERROR'] = curl_error($ch);
            $result['STATUS'] = self::STATUS_ERROR;
        } else {
            $result['DATA'] = $response;
            $result['STATUS'] = self::STATUS_OK;
        }
        curl_close($ch);

        return $result;
    }

    private function getAvailable()
    {
        $isTestMode = Options::isTest();
        $arServers = array();
        if ($isTestMode) {
            $dbServer = ServerTable::getList(array(
                "filter" => array("TYPE_SERVER" => "test"),
                "order" => array("TIME_UPDATE" => "desc"),
            ));
        } else {
            $dbServer = ServerTable::getList(array(
                "filter" => array("TYPE_SERVER" => "live"),
                "order" => array("TIME_UPDATE" => "desc"),
            ));
        }

        while ($itemServer = $dbServer->fetch()) {
            $arServers[] = $itemServer;
        }

        return $arServers;
    }
}
