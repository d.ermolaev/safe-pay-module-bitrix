<?php
namespace SafePay\Blockchain;

class Agent
{
    public function resultPay()
    {
        $procces = new Procces();
        $procces->resultPay();
        return "\SafePay\Blockchain\Agent::resultPay();";
    }

    public function availableServers()
    {
        $dbServer = \SafePay\Blockchain\Entitys\ServerTable::getList(array());
        while ($arServer = $dbServer->fetch()) {
            $url = $arServer["URL_SERVER"] . "/api/lastblock";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPGET, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            $response = curl_exec($ch);
            curl_close($ch);
            $result =  (array) json_decode($response, true);

            if ($result["timestamp"] > $arServer["TIME_UPDATE"]) {
                \SafePay\Blockchain\Entitys\ServerTable::Update($arServer["ID"], array(
                    'TIME_UPDATE' => $result["timestamp"],
                ));
            }
        }

        return "\SafePay\Blockchain\Agent::availableServers();";
    }
}