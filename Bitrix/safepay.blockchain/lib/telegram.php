<?php
namespace SafePay\Blockchain;

use Bitrix\Main\Application;
use Bitrix\Main\Web\Json;
use SafePay\Blockchain\Entitys\RecipientTable;
use SafePay\Blockchain\Helpers\Base58;
use SafePay\Blockchain\Helpers\ConvertToBytes;

class Telegram
{
    private $recipient;

    public function __construct($recipient)
    {
        $this->recipient = RecipientTable::getByAtribute($recipient);
    }
    
    public function get($params, $type)
    {
        $result = array();
        $data0 = $this->getData0();
        $data1 = $this->getData1($params, $type);
        $result['signature'] = $this->getSign($data0, $data1);
        $result['raw'] = $this->getRaw($data0, $data1, $result['signature']);
        return $result;
    }

    private function getSign($data0, $data1)
    {
        $port = (Options::isTest()) ? 9066 : 9046;
        $dataForSign = array_merge($data0, $data1, ConvertToBytes::fromInt32($port));
        $dataForSign = Base58::encode($dataForSign);
        $cp = new Cryptobase();
        $sign = $cp->sign($dataForSign, Options::publicKey(), Options::privateKey());
        return $sign['signature'];
    }

    private function getRaw($data0, $data1, $sign)
    {
        return Base58::encode(array_merge($data0, Base58::decode($sign), $data1));
    }

    private function getData0()
    {
        $tiemstamp = round(microtime(true) * 1000);

        //TRANSACTION_TYPE
        $data0 = array(31, 0, 0, 0);
        //TIMESTAMP
        $data0 = array_merge($data0, ConvertToBytes::fromInt64($tiemstamp));
        //REFERENCE
        $data0 = array_merge($data0, array(0, 0, 0, 0, 0, 0, 0, 0));
        //CREATOR PUBLIC KEY
        $data0 = array_merge($data0, Base58::decode(Options::publicKey()));
        //FEE POW
        $data0 = array_merge($data0, array(0));

        return $data0;
    }

    private function getData1($params, $type)
    {
        $data1 = array();
        //RECIPIENT
        $data1 = Base58::decode($this->recipient['BILD']);
        //ASSET KEY
        //$data1 = array_merge($data1, ConvertToBytes::fromInt64($params['curr']));
        $data1 = array_merge($data1, array(0, 0, 0, 0, 0, 0, 0, 0));
        //AMOUNT
        //$data1 = array_merge($data1, ConvertToBytes::fromInt64($params['sum'] * 100000000));
        $data1 = array_merge($data1, array(0, 0, 0, 0, 0, 0, 0, 0));

        $titleByte = ConvertToBytes::fromString($params["userPhone"]);
        //TITLE LENGTH
        $data1 = array_merge($data1, array(count($titleByte)));
        //TITLE
        $data1 = array_merge($data1, $titleByte);

        $message = '';
        if ($type == 'new') {
            $message = $this->getMessage($params);
        } elseif ($type == 'delete') {
            $message = $this->getMessageDelete($params);
        }
        //MESSAGE LENGTH
        $data1 = array_merge($data1, ConvertToBytes::fromInt32(count($message)));
        //MESSAGE
        $data1 = array_merge($data1, $message);
        //ENCRYPTED
        if ($params['oldSign'] || $type == 'delete') {
            $data1 = array_merge($data1, array(0));
        } else {
            $data1 = array_merge($data1, array(1));
        }
        //TEXT
        $data1 = array_merge($data1, array(1));

        return $data1;
    }

    private function getMessage($params)
    {
        $arMessage = array(
            'date' => $params['order_date'],
            'order' => $params['order_num'],
            'user' => $params['userPhone'],
            'curr' => $params['curr'],
            'sum' => $params['sum'],
            'expire' => strtotime($params['expire']),
            'title' => $params['title'],
            'description' => $params['description'],
        );

        if ($params['oldSign']) {
            $arMessage['details'] = 'delete,' . $params['oldSign'];
            $arMessage['callback'] = $this->getUrlCallback($params);
            return ConvertToBytes::fromString(Json::encode($arMessage, JSON_UNESCAPED_UNICODE));
        } else {
            $arMessage['details'] = '-';
            $arMessage['callback'] = $this->getUrlCallback($params);
            return $this->cryptMessage($arMessage);
        }
    }

    private function getMessageDelete($params)
    {
        $arMessage = array(
            "__DELETE" => array(
                "list" => array($params['BANK_ID'])
            )
        );

        return ConvertToBytes::fromString(Json::encode($arMessage, JSON_UNESCAPED_UNICODE));
    }

    private function cryptMessage(array $arMessage)
    {
        $jsonMessage = Json::encode($arMessage, JSON_UNESCAPED_UNICODE);
        $cp = new Cryptobase();
        $encryptMessage = $cp->encrypt($jsonMessage, $this->recipient['PUBLIC_KEY'], Options::privateKey());
        return Base58::decode($encryptMessage['encrypted']);
    }

    private function getUrlCallback($params)
    {
        $context = Application::getInstance()->getContext();
        $server = $context->getServer();
        $request = $context->getRequest();
        $url = $request->isHttps() ? "https://" : "http://";
        $url .= $server->getHttpHost() . '/bitrix/safepay.blockchain/result.php';
        return $url;
    }
}
