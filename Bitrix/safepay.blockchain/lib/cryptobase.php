<?php
namespace SafePay\Blockchain;

use SafePay\Blockchain\Helpers\Base58;
use SafePay\Blockchain\Helpers\ConvertToBytes;
use SafePay\Blockchain\Sodium\Core\ParagonIE_Sodium_Core_Ed25519;
use SafePay\Blockchain\Sodium\ParagonIE_Sodium_Crypto;
use SafePay\Blockchain\Sodium\ParagonIE_Sodium_Compat;

class Cryptobase
{
    const SEED_BYTES = 32;
    const METHOD_CRYPT = 'aes-256-cbc';
    private $iv;

    public function __construct()
    {
        $this->iv = chr(0x06) . chr(0x04) . chr(0x03) . chr(0x08) . chr(0x01) . chr(0x02) . chr(0x01) . chr(0x02)
        . chr(0x07) . chr(0x02) . chr(0x03) . chr(0x08) . chr(0x05) . chr(0x07) . chr(0x01) . chr(0x01);
    }

    public function generateSeed()
    {
        include_once("random_bytes/random.php");
        
        $seed = random_bytes(self::SEED_BYTES);
        $arSeed = array(
            'seed' => $seed,
            'seed_base58' => Base58::encode(ConvertToBytes::fromString($seed)),
        );
        return $arSeed;
    }

    public function generateKeyPair($seed_base58 = null)
    {
        if (!$seed_base58) {
            $seed = $this->generateSeed();
        } else {
            $seed = array(
                'seed' => ConvertToBytes::toString(Base58::decode($seed_base58)),
                'seed_base58' => $seed_base58,
            );
        }
        $pk = '';
        $sk = '';
        $keyPair = ParagonIE_Sodium_Core_Ed25519::seed_keypair($pk, $sk,$seed['seed']);
        $result = array(
            'seed' => $seed['seed_base58'],
            'privateKey' => Base58::encode(ConvertToBytes::fromString($sk)),
            'publicKey' => Base58::encode(ConvertToBytes::fromString($pk)),

        );
        return $result;
    }

    public function encrypt($message, $publicKey, $privateKey)
    {
        $password = self::getPassword($publicKey, $privateKey);
        $encrypted = openssl_encrypt($message, self::METHOD_CRYPT, $password, OPENSSL_RAW_DATA, $this->iv);
        $era_encrypted = chr(0x01) . $encrypted;
        $result = array(
            'encrypted' => Base58::encode(ConvertToBytes::fromString($era_encrypted)),
        );
        return $result;
    }

    public function decrypt($message, $publicKey, $privateKey)
    {
        $password = self::getPassword($publicKey, $privateKey);
        $era_encrypted = ConvertToBytes::toString(Base58::decode($message));
        $encrypted = substr($era_encrypted, 1, strlen($era_encrypted) - 1);
        $decrypted = openssl_decrypt($encrypted, self::METHOD_CRYPT, $password, OPENSSL_RAW_DATA, $this->iv);
        $result = array(
            'decrypted' => $decrypted,
        );
        return $result;
    }

    private function getPassword($publicKey, $privateKey)
    {
        $publicKey = ConvertToBytes::toString(Base58::decode($publicKey));
        $privateKey = ConvertToBytes::toString(Base58::decode($privateKey));
        $publicKey_curve25519 = ParagonIE_Sodium_Core_Ed25519::pk_to_curve25519($publicKey);
        $privateKey_curve25519 = ParagonIE_Sodium_Compat::crypto_sign_ed25519_sk_to_curve25519($privateKey);
        $ss = ParagonIE_Sodium_Crypto::scalarmult($privateKey_curve25519, $publicKey_curve25519);
        $password = substr(hash('sha256', $ss, true),0, 32);
        return $password;
    }

    public function sign($message, $publicKey, $privateKey)
    {
        $message = ConvertToBytes::toString(Base58::decode($message));
        $privateKey = ConvertToBytes::toString(Base58::decode($privateKey));
        $sign = ParagonIE_Sodium_Core_Ed25519::sign_detached($message, $privateKey);
        $result = array(
            'signature' => Base58::encode(ConvertToBytes::fromString($sign)),
        );
        return $result;
    }

    public function verifySignature($message, $signature, $publicKey)
    {
        $message = ConvertToBytes::toString(Base58::decode($message));
        $signature = ConvertToBytes::toString(Base58::decode($signature));
        $publicKey = ConvertToBytes::toString(Base58::decode($publicKey));
        $verifySignature = ParagonIE_Sodium_Core_Ed25519::verify_detached($signature, $message, $publicKey);
        $result = array(
            'signatureVerify' => $verifySignature,
        );
        return $result;
    }

    public function generateAccount($seed = false, $nonce = false)
    {
        if (!$seed) {
            $arSeed = $this->generateSeed();
            $seed = $arSeed['seed_base58'];
        }
        if (!$nonce) {
            $nonce = time();
        }
        $accountSeed = $this->getAccountSeed($seed, $nonce);
        $keyPair = $this->generateKeyPair($accountSeed);

        $result = array(
            'seed' => $seed,
            'accountSeed' => $accountSeed,
            'privateKey' => $keyPair['privateKey'],
            'numAccount' => $nonce,
            'publicKey' => $keyPair['publicKey'],
            'privateKey_CP' => $this->changeKeyToProvider($keyPair['privateKey']),
            'account' => $this->genAccount($publicKey),
        );
        return $result;
    }

    private function getAccountSeed($seed_base58, $nonce)
    {
        $result = array();
        $nonce = $nonce - 1;
        $nonce_byte = ConvertToBytes::fromInt32($nonce);
        $result = array_merge($result, $nonce_byte);
        $seed_byte = Base58::decode($seed_base58);
        $result = array_merge($result, $seed_byte);
        $result = array_merge($result, $nonce_byte);
        $hash_res = hash('sha256', ConvertToBytes::toString($result), true);
        $hash_hash_res = hash('sha256', $hash_res, true);
        return Base58::encode(ConvertToBytes::fromString($hash_hash_res));
    }

    private function genAccount($pk)
    {
        $pk_hash_sha256 =  hash('sha256', $pk, true);
        $pk_hash_ripemd160 = hash('ripemd160', $pk_hash_sha256, true);
        $result = array();
        $result[] = 15;
        $result = array_merge($result, ConvertToBytes::fromString($pk_hash_ripemd160));
        $hash_res = hash('sha256', ConvertToBytes::toString($result), true);
        $hash_hash_res = hash('sha256', $hash_res, true);
        $hash_hash_res_arr = ConvertToBytes::fromString($hash_hash_res);
        $result = array_merge($result, array_slice($hash_hash_res_arr, 0 , 4));
        return Base58::encode(($result));
    }

    private function changeKeyToProvider($secretkey)
    {
        $secretkey = Base58::decode($secretkey);
        $secretkey = array_slice($secretkey, 0, 32);
        $secretkey = hash('sha512', ConvertToBytes::toString($secretkey), true);
        $secretkey = ConvertToBytes::fromString($secretkey);
        $first_byte = $secretkey[0];
        $first_byte &= 248;
        $secretkey[0] = $first_byte;
        $last_byte = $secretkey[31];
        $last_byte &= 63;
        $last_byte |= 64;
        $secretkey[31] = $last_byte;
        $secretkey = array_merge($secretkey);
        return Base58::encode($secretkey);
    }
}
