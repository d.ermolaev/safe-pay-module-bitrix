<?php
namespace SafePay\Blockchain;

use Bitrix\Main\Application;
use SafePay\Blockchain\Entitys\InvoiceTable;
use SafePay\Blockchain\Entitys\RecipientTable;
use SafePay\Blockchain\Helpers\Json;
use SafePay\Blockchain\Helpers\Request;
use SafePay\Blockchain\Helpers\Security;

class Procces
{
    const STATUS_OK = 'OK';
    const STATUS_ERROR = 'ERROR';

    public function initPay($params, $recipient)
    {
        if ($arInvoice = InvoiceTable::isActive($params['order'])) {
            $result['STATUS'] = self::STATUS_OK;
            return $result;
        }

        $result = array();
        $telegram = new Telegram($recipient);
        $data = $telegram->get($params, 'new');
        $url = "/api/broadcasttelegram/" . $data['raw'];
        $requestHelpers = new Request();
        $response = $requestHelpers->send($url);
        if ($response['STATUS'] == Request::STATUS_OK) {
            $dataResult = Json::decode($response['DATA']);
            if ($dataResult && $dataResult['status'] == 'ok') {
                $result['STATUS'] = self::STATUS_OK;

                $requestApp = Application::getInstance()->getContext()->getRequest();
                $serverApp = Application::getInstance()->getContext()->getServer();
                $siteURL = $requestApp->isHttps() ? "https://" . $serverApp->getHttpHost() : "http://" . $serverApp->getHttpHost();

                InvoiceTable::add(array(
                    'STATUS' => InvoiceTable::STATUS_ACTIVE,
                    'RECIPIENT' => $recipient,
                    'BANK_ID' => $data['signature'],
                    'EXPIRE' => strtotime($params['expire']),
                    'PAY_NUM' => $params['order'],
                    'IS_TEST' => Options::isTest(),
                    'PAYSYSTEM_ID' => $params['PAYSYSTEM_ID'],
                    'PAYSYSTEM_TYPE' => $params['PAYSYSTEM_TYPE'],
                    'CREATOR' => serialize($params),
                    'DATE_CREATED' => date("d.m.Y H:i:s"),
                    'SITE_URL' => $siteURL,
                ));

            } else {
                $result['STATUS'] = self::STATUS_ERROR;
                $result['ERROR'] = $response['DATA'];
            }
        } else {
            $result['STATUS'] = self::STATUS_ERROR;
            $result['ERROR'] = $response['ERROR'];
        }

        return $result;
    }

    public function resultPay($signSearch = false)
    {
        $arResult = false;

        InvoiceTable::deactiveExpiring();

        $listInvoiceFast = $this->resultPayFast();

        $next = Options::getNextStep();
        $height = $next;
        $listInvoiceChain = $this->resultPayChain($next, $height);

        $listInvoice = array_merge($listInvoiceFast, $listInvoiceChain);

        if (count($listInvoice) == 0) {
            Options::setNextStep($height);
            return false;
        }

        $cp = new Cryptobase();
        $arRecipients = RecipientTable::getListAll();

        $arSign = array();
        foreach ($listInvoice as $itemInvoice) {
            $arSign[] = $itemInvoice['orderSignature'];
        }
        $dbInvoice = InvoiceTable::getList(array('filter' => array('STATUS' => InvoiceTable::STATUS_ACTIVE, 'BANK_ID' => $arSign)));
        $shopInvoice = array();
        while ($arInvoice = $dbInvoice->fetch()) {
            $shopInvoice[$arInvoice['BANK_ID']] = $arInvoice;
        }

        if (count($shopInvoice) == 0) {
            Options::setNextStep($height);
            return false;
        }

        if (!Bitrix\Main\Loader::IncludeModule("sale")) {
            return false;
        }

        foreach ($listInvoice as $key => $itemInvoice) {
            $signature = $itemInvoice['orderSignature'];
            $arInvoice = $shopInvoice[$signature];
            if (!$arInvoice || $itemInvoice['creator'] != $arRecipients[$arInvoice['RECIPIENT']]) {
                continue;
            }

            //InvoiceTable::updateStatus($arInvoice["ID"], InvoiceTable::STATUS_WAITING);

            //Перевести эти заказы в оплаченное состояние
            $secret = $cp->encrypt(
                $signature . round($itemInvoice['sum']),
                Options::publicKey(),
                Options::privateKey()
            );
            $arParam = array(
                'sign' => $signature,
                'sum' => $itemInvoice['sum'],
                'pay' => $key,
                'secret' => $secret['encrypted'],
            );
            $arParam2 = array(
                'SAFEPAY_HANDLER_ID' => $arInvoice["PAYSYSTEM_ID"],
                'SAFEPAY_HANDLER_NAME' => $arInvoice["PAYSYSTEM_TYPE"],
                'PAYMENT_ID' => $arInvoice["PAY_NUM"],
            );
            $serverPay = \Bitrix\Main\Application::getInstance()->getContext()->getServer();
            $requestPay = new \SafePay\Blockchain\Helpers\BRequest($serverPay, array_merge($arParam2, Security::encode($arParam)));
            $servicePay = \Bitrix\Sale\PaySystem\Manager::getObjectById($itemInvoice['PAYSYSTEM_ID']);
            $resultPay = $servicePay->processRequest($requestPay);
            if ($resultPay->isSuccess()) {
                InvoiceTable::updateStatus($arInvoice["ID"], InvoiceTable::STATUS_FINISH);
            }
        }

        if ($signSearch) {
            $dbInvoice = InvoiceTable::getList(array(
                'filter' => array(
                    'STATUS' => InvoiceTable::STATUS_FINISH,
                    'BANK_ID' => $signSearch,
                )));
            if ($arTempInvoice = $dbInvoice->fetch()) {
                $arResult = $arTempInvoice;
            }
        }

        Options::setNextStep($height);

        return $arResult;
    }

    private function resultPayFast()
    {
        $result = array();
        $bild = Options::bild();
        $url = "/apirecords/unconfirmedincomes/" . $bild;
        $requestHelpers = new Request();
        $response = $requestHelpers->send($url, array('type' => 31, 'descending' => 'true'));
        if ($response['STATUS'] == Request::STATUS_OK) {
            if (strlen($response['DATA']) > 0) {
                $data = Json::decode($response['DATA']);
                if ($data) {
                    foreach ($data as $item) {
                        $tempData = Json::decode($item['message']);
                        if ($tempData) {
                            $result[$item['signature']] = array(
                                'orderSignature' => $tempData['orderSignature'],
                                'sum' => $tempData['sum'],
                                'creator' => $item['creator'],
                            );
                        }
                    }
                }
            }
        }
        return $result;
    }

    private function resultPayChain($next, &$height)
    {
        $result = array();

        $bild = Options::bild();
        $requestHelpers = new Request();

        do {
            $url = "/apirecords/incomingfromblock/" . $bild . "/" . $next;
            $response = $requestHelpers->send($url);
            if ($response['STATUS'] == Request::STATUS_OK) {
                $data = Json::decode($response['DATA']);
                if (!$data) {
                    $next = false;
                } elseif (array_key_exists('next', $data)) {
                    $next = $data['next'];
                    $height = $next;
                    //Options::setNextStep($next);
                } elseif (array_key_exists('height', $data)) {
                    $height = $data['height'];
                    //Options::setNextStep($height);
                    $next = false;
                } else {
                    $next = false;
                }
                if ($data && array_key_exists('txs', $data) && count($data['txs']) > 0) {
                    foreach ($data['txs'] as $item) {
                        $tempData = Json::decode($item['message']);
                        if ($tempData) {
                            $result[$item['signature']] = array(
                                'orderSignature' => $tempData['orderSignature'],
                                'sum' => $tempData['sum'],
                                'creator' => $item['creator'],
                            );
                        }
                    }
                }
            } else {
                $next = false;
            }
        } while ($next);
        return $result;
    }

    public function canceledPay($id)
    {
        self::resultPay();

        $dbInvoice = InvoiceTable::getList(array(
            'filter' => array(
                'ID' => $id,
                'STATUS' => 'active',
            ),
        ));

        if ($arInvoice = $dbInvoice->fetch()) {
            $requestHelpers = new Request();
            $urlCheck = '/apitelegrams/check/' . $arInvoice['BANK_ID'];
            $resCheck = $requestHelpers->send($urlCheck);
            $check = Json::decode($resCheck['DATA']);
            if ($check) {
                $arProp = unserialize($arInvoice["CREATOR"]);
                $telegram = new Telegram();
                $params = array(
                    "userPhone" => $arProp['userPhone'],
                    "BANK_ID" => $arInvoice['BANK_ID'],
                );
                $data = $telegram->get($params, 'delete');
                $urlCancel = "/api/broadcasttelegram/" . $data['raw'];
                $responseCanceled = $requestHelpers->send($urlCancel, array('broadcast' => 'true'));
                $dataResult = ($responseCanceled['STATUS'] == Request::STATUS_OK)
                ? Json::decode($responseCanceled['DATA'])
                : "";

                if ($dataResult['status'] === "ok") {
                    InvoiceTable::Update($id, array(
                        'STATUS' => 'canceled',
                        'DATE_UPDATED' => date("d.m.Y H:i:s"),
                    ));
                    return true;
                }

            }
        }

        return false;
    }

    public function refundPay($id)
    {
        $dbInvoice = InvoiceTable::getList(array(
            'filter' => array('ID' => $id),
        ));

        if ($arInvoice = $dbInvoice->fetch()) {
            $params = unserialize($arInvoice['CREATOR']);
            $params['oldSign'] = $arInvoice['BANK_ID'];
            $telegram = new Telegram();
            $data = $telegram->get($params);
            $url = "/api/broadcasttelegram/" . $data['raw'];

            $request = new Request();
            $response = $request->send($url);
            if ($response['STATUS'] == Request::STATUS_OK) {
                $dataResult = Json::decode($response['DATA']);
                if ($dataResult && $dataResult['status'] == 'ok') {
                    $result['STATUS'] = self::STATUS_OK;

                    InvoiceTable::Update($id, array(
                        'STATUS' => 'refund',
                        'DATE_UPDATED' => date("d.m.Y H:i:s"),
                    ));

                    return true;
                }
            }
        }

        return false;
    }
}
