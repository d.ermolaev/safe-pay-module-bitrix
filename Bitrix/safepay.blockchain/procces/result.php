<?php
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use SafePay\Blockchain\Procces;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define("DisableEventsCheck", true);
require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";

$request = Application::getInstance()->getContext()->getRequest();

if (Loader::IncludeModule("safepay.blockchain")) {
    $procces = new Procces();
    $result = $procces->resultPay($request['sign']);
    if ($request['redirect'] && $result) {
        LocalRedirect('/bitrix/safepay.blockchain/page/success.php', true);
    } elseif ($request['redirect']) {
        LocalRedirect('/bitrix/safepay.blockchain/page/fail.php', true);
    }
}

require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php";
