<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use SafePay\Blockchain\Entitys\RecipientTable;
use SafePay\Blockchain\Helpers\Security;
use SafePay\Blockchain\Procces;

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
define("DisableEventsCheck", true);
require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";

Loc::loadMessages(__FILE__);

if (Loader::IncludeModule("safepay.blockchain")) {
    $request = Application::getInstance()->getContext()->getRequest();
    $recipient = $request['RECIPIENT'];

    $arDecodeParams = Security::decode($request['DATA']);
    if ($arDecodeParams['HASH'] !== $request['HASH']) {
        echo Loc::getMessage("SAFEPAY_BLOCKCHAIN_error_hash");
    } else {
        $procces = new Procces();
        $result = $procces->initPay($arDecodeParams['DATA'], $recipient);
        if ($result['STATUS'] == Procces::STATUS_OK) {
            $serverApp = Application::getInstance()->getContext()->getServer();
            $userAgent = strtolower($serverApp['HTTP_USER_AGENT']);
            $dataRecipient = RecipientTable::getByAtribute($recipient);
            if (
                stripos($userAgent, 'android') !== false
                && $dataRecipient['APP_ANDROID'] !== false
            ) {
                LocalRedirect('https://play.google.com/store/apps/details?id=' . $dataRecipient['APP_ANDROID'], true);
            } elseif (
                stripos($userAgent, 'iphone') !== false
                && $dataRecipient['APP_IOS'] !== false
            ) {
                LocalRedirect('https://itunes.apple.com/' . $dataRecipient['APP_IOS'], true);
            } else {
                LocalRedirect($dataRecipient['PAY_URL'], true);
            }

        } else {
            LocalRedirect('/bitrix/safepay.blockchain/page/ready_fail.php', true);
        }
    }
} else {
    echo "Error: module 'safepay.blockchain' not install.";
}
