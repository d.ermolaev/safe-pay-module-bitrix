<?php
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$data = array(
    'NAME' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_NAME'),
    'SORT' => 500,
    'CODES' => array(

        //ORDER_INFO
        'userPhone' => array(
            'NAME' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_userPhone'),
            'DESCRIPTION' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_DESC_userPhone'),
            'SORT' => 100,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO'),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'PROPERTY',
            ),
        ),
        'order_num' => array(
            "NAME" => Loc::getMessage('SAFEPAY_BLOCKCHAIN_order_num'),
            'SORT' => 200,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO'),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'ORDER',
                'PROVIDER_VALUE' => 'ACCOUNT_NUMBER',
            ),
        ),
        'order' => array(
            "NAME" => Loc::getMessage('SAFEPAY_BLOCKCHAIN_order'),
            'SORT' => 205,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO'),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'PAYMENT',
                'PROVIDER_VALUE' => 'ID',
            ),
        ),
        'order_date' => array(
            "NAME" => Loc::getMessage('SAFEPAY_BLOCKCHAIN_order_date'),
            'SORT' => 210,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO'),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'ORDER',
                'PROVIDER_VALUE' => 'DATE_INSERT',
            ),
        ),
        'sum' => array(
            "NAME" => Loc::getMessage('SAFEPAY_BLOCKCHAIN_sum'),
            'SORT' => 215,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO'),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'PAYMENT',
                'PROVIDER_VALUE' => 'SUM',
            ),
        ),
        'sum_tax' => array(
            "NAME" => Loc::getMessage('SAFEPAY_BLOCKCHAIN_sum_tax'),
            'SORT' => 220,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO'),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'ORDER',
                'PROVIDER_VALUE' => 'TAX_VALUE',
            ),
        ),
        'curr_b' => array(
            'NAME' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_curr_b'),
            'SORT' => 225,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO'),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'PAYMENT',
                'PROVIDER_VALUE' => 'CURRENCY',
            ),
        ),
        'curr' => array(
            'NAME' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_curr'),
            'SORT' => 230,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO'),
            'DEFAULT' => array(
                'PROVIDER_KEY' => 'VALUE',
                "PROVIDER_VALUE" => "643",
            ),
        ),
        //MORE_PARAM
        'expire' => array(
            'NAME' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_expire'),
            'DESCRIPTION' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_DESC_expire'),
            'SORT' => 300,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_MORE_PARAM'),
        ),
        'title' => array(
            'NAME' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_title'),
            'DESCRIPTION' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_DESC_title'),
            'SORT' => 305,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_MORE_PARAM'),
        ),
        'description' => array(
            'NAME' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_description'),
            'DESCRIPTION' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_DESC_description'),
            'SORT' => 310,
            'GROUP' => Loc::getMessage('SAFEPAY_BLOCKCHAIN_TAB_MORE_PARAM'),
        ),

    ),
);
