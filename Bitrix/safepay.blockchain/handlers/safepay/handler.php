<?php
namespace Sale\Handlers\PaySystem;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Request;
use Bitrix\Main\Type\DateTime;
use Bitrix\Sale\Payment;
use Bitrix\Sale\PaySystem;
use SafePay\Blockchain\Cryptobase;
use SafePay\Blockchain\Entitys\InvoiceTable;
use SafePay\Blockchain\Entitys\RecipientTable;
use SafePay\Blockchain\Helpers\Security;
use SafePay\Blockchain\Options;
use SafePay\Blockchain\Procces;

Loc::loadMessages(__FILE__);

class safepayHandler extends PaySystem\ServiceHandler
{
    public static function getIndicativeFields()
    {
        return array('SAFEPAY_HANDLER_NAME' => 'SAFEPAYHANDLER');
    }

    protected function getUrlList()
    {
        return array();
    }

    public function getPaymentIdFromRequest(Request $request)
    {
        return $request->get('PAYMENT_ID');
    }

    protected static function isMyResponseExtended(Request $request, $paySystemId)
    {
        $id = $request->get('SAFEPAY_HANDLER_ID');
        return $id == $paySystemId;
    }

    protected function isTestMode(Payment $payment = null)
    {
        return (Option::get('safepay.blockchain', 'isTest', '') == 'Y');
    }

    public function getCurrencyList()
    {
        return array();
    }

    protected function getPhone($phone)
    {
        $phone = preg_replace('~[^0-9]+~', '', $phone);
        if (strlen($phone) > 0) {
            $phone = ($phone[0] == '7' || $phone[0] == '8') ? substr($phone, 1) : $phone;
        }
        return $phone;
    }

    protected function getPaySystemSettings($paymentID, $field)
    {
        $paySystemService = PaySystem\Manager::getObjectById($paymentID);
        $arPaySysAction = $paySystemService->getFieldsValues();
        return $arPaySysAction[$field];
    }

    private function packParams($params, $paymentSystemId)
    {
        $params['PAYSYSTEM_ID'] = $paymentSystemId;
        $params['PAYSYSTEM_TYPE'] = 'SAFEPAYHANDLER';
        $params['userPhone'] = $this->getPhone($params['userPhone']);
        $params['order_date'] = strtotime($params['order_date']->toString());
        $params['sum'] = round($params['sum'], 2);
        $params['curr'] = intval($params['curr']);
        $params['title'] = strlen($params['title']) > 0 ? $params['title'] : '-';
        $params['description'] = $this->getDescriptionSP($params);
        $params['expire'] = (intval($params['expire']) > 0) ? "+" . $params['expire'] . " hours" : "+24 hours";

        return Security::encode($params);
    }

    private function getDescriptionSP($params)
    {
        $template = (strlen($params['description']) > 0)
        ? $params['description']
        : Loc::getMessage('SAFEPAY_BLOCKCHAIN_description_tempalte');

        $template = str_replace('#ORDER_NUM#', $params['order_num'], $template);
        $template = str_replace('#ORDER_DATE#', date('d.m.Y', $params['order_date']), $template);
        if ($params['sum_tax'] > 0) {
            $tax = str_replace('#TAX#', Loc::getMessage('SAFEPAY_BLOCKCHAIN_description_tax'), $template);
            $template = str_replace('#ORDER_TAX#', $tax, $template);
        } else {
            $template = str_replace('#ORDER_TAX#', Loc::getMessage('SAFEPAY_BLOCKCHAIN_description_without_tax'), $template);
        }

        return $template;
    }

    private function validParams($params)
    {
        $result = array();

        if (strlen(Options::publicKey()) == 0) {
            $result[] = Loc::getMessage('SAFEPAY_BLOCKCHAIN_error_publicKey');
        }
        if (strlen(Options::privateKey()) == 0) {
            $result[] = Loc::getMessage('SAFEPAY_BLOCKCHAIN_error_privateKey');
        }
        if (strlen(Options::bild()) == 0) {
            $result[] = Loc::getMessage('SAFEPAY_BLOCKCHAIN_error_bild');
        }
        if (strlen($params["order"]) == 0) {
            $result[] = Loc::getMessage('SAFEPAY_BLOCKCHAIN_error_order');
        }
        if (strlen($params["userPhone"]) == 0) {
            $result[] = Loc::getMessage('SAFEPAY_BLOCKCHAIN_error_userPhone');
        }
        if (strlen($params["sum"]) == 0) {
            $result[] = Loc::getMessage('SAFEPAY_BLOCKCHAIN_error_sum');
        }
        if (strlen($params["curr"]) == 0) {
            $result[] = Loc::getMessage('SAFEPAY_BLOCKCHAIN_error_curr');
        }
        if (strlen($params["description"]) == 0) {
            $result[] = Loc::getMessage('SAFEPAY_BLOCKCHAIN_error_description');
        }

        return $result;
    }

    public function initiatePay(Payment $payment, Request $request = null)
    {
        Loader::includeModule('safepay.blockchain');
        $paymentSystemId = $payment->getPaymentSystemId();
        $params = $this->getParamsBusValue($payment);

        if ($arInvoice = InvoiceTable::isActive($params['order'])) {
            $bank = RecipientTable::getByAtribute($arInvoice["RECIPIENT"]);
            $settings = array(
                'SIGN' => $arInvoice['BANK_ID'],
                'NAME_BANK' => $bank['NAME'],
                'URL_INIT' => '/bitrix/safepay.blockchain/result.php',
                'NEW_WINDOW' => $this->getPaySystemSettings($paymentSystemId, 'NEW_WINDOW'),
            );
        } else {
            $arError = $this->validParams($params);
            $arParams = $this->packParams($params, $paymentSystemId);
            $recipient = RecipientTable::GetList()->fetchAll();
            $settings = array(
                'DATA' => $arParams['DATA'],
                'HASH' => $arParams['HASH'],
                'RECIPIENT' => $recipient,
                'ERRORS' => $arError,
                'URL_INIT' => '/bitrix/safepay.blockchain/init.php',
                'NEW_WINDOW' => $this->getPaySystemSettings($paymentSystemId, 'NEW_WINDOW'),
            );
        }
        $this->setExtraParams($settings);
        return $this->showTemplate($payment, 'template');
    }

    public function processRequest(Payment $payment, Request $request)
    {
        Loader::includeModule("safepay.blockchain");
        $result = new PaySystem\ServiceResult();
        $params = $this->getParamsBusValue($payment);
        $procces = new Procces();
        $resultProc = $this->resultPayTest($request['DATA'], $request['HASH'], $params);
        if ($resultProc['STATUS'] == Procces::STATUS_OK) {
            $result = $this->processNoticeAction($payment, $request);
            echo 'ok';
        } elseif ($resultProc['STATUS'] == Procces::STATUS_ERROR) {
            $errorMessage = 'Problem with pay';
            $result->addError(new Error($errorMessage));
            PaySystem\ErrorLog::add(array(
                'ACTION' => 'processRequest',
                'MESSAGE' => $errorMessage,
            ));
        }
        return $result;
    }

    private function processNoticeAction(Payment $payment, Request $request)
    {
        $result = new PaySystem\ServiceResult();
        $fields = array(
            "PS_STATUS" => "Y",
            "PS_STATUS_DESCRIPTION" => "-",
            "PS_STATUS_MESSAGE" => "",
            "PS_RESPONSE_DATE" => new DateTime(),
        );
        $result->setPsData($fields);
        $result->setOperationType(PaySystem\ServiceResult::MONEY_COMING);

        return $result;
    }

    public function resultPayTest($data, $hash, $params)
    {
        $data = Security::decode($data);
        if ($data['HASH'] == $hash) {
            $arInvoice = InvoiceTable::getList(array('filter' => array('BANK_ID' => $data['DATA']['sign'])))->fetch();
            if ($arInvoice) {
                $cp = new Cryptobase();
                $dataDecrypted = $cp->decrypt($data['DATA']['secret'], Options::publicKey(), Options::privateKey());
                if (strlen($dataDecrypted['decrypted']) > 0 && $dataDecrypted['decrypted'] == $data['DATA']['sign'] . round($params['sum'])) {
                    if ($this->isCorrectSum($data['DATA']['sum'], $params['sum'], $params['curr_b'])) {
                        return array("ID" => $arInvoice['ID'], "STATUS" => Procces::STATUS_OK, 'DATA' => $data['DATA']);
                    }
                }

            }
        }
        return false;
    }

    private function isCorrectSum($sum1, $sum2, $currency)
    {
        return $this->roundByFormatCurrency($sum1, $currency) == $this->roundByFormatCurrency($sum2, $currency);
    }

    private function roundByFormatCurrency($price, $currency)
    {
        return floatval(SaleFormatCurrency($price, $currency, false, true));
    }
}
