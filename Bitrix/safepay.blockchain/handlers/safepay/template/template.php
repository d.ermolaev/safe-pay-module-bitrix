<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages("/bitrix/safepay.blockchain/handlers/safepay/lang/ru/template/template.php");
?>
<?if (strlen($params["SIGN"]) > 0) :?>
    <p>Оплата ожидается через <?=$params["NAME_BANK"]?></p>
    <form id="payForm" action="<?=$params["URL_INIT"]?>" method="post">
        <input type="hidden" name="sign" value="<?=$params["SIGN"]?>" />
        <input type="hidden" name="redirect" value="Y" />
        <?if($params["NEW_WINDOW"] != "Y"):?>
            <input type="submit" value="<?=Loc::getMessage('SAFEPAY_BLOCKCHAIN_BTN_CHECK_PAY')?>" />
        <?endif?>
    </form>
    <?if($params["NEW_WINDOW"] == "Y"):?>
        <script>
            document.getElementById("payForm").submit();
        </script>
    <?endif?>
<?elseif (is_array($params["ERRORS"]) && count($params["ERRORS"]) > 0) :?>
    <?foreach ($params["ERRORS"] as $error) :?>
        <?=$error.'<br/>'?>
    <?endforeach?>
<?else :?>
    <form id="payForm" action="<?=$params["URL_INIT"]?>" method="post">
        <input type="hidden" name="DATA" value="<?=$params["DATA"]?>" />
        <input type="hidden" name="HASH" value="<?=$params["HASH"]?>" />
        <p><?=Loc::getMessage('SAFEPAY_BLOCKCHAIN_TEXT_SELECTED_PAY')?></p>
        <ul class="wrap-select-bank">
        <?foreach ($params["RECIPIENT"] as $key=>$item) :?>
            <li>
                <input id='RECIPIENT_<?=$key?>' name='RECIPIENT' type='radio' value='<?=$item['ATRIBUTE']?>' <?if($key == 0):?>checked='checked'<?endif?> />
                <label for='RECIPIENT_<?=$key?>'>
                    <div class='wrap-img'><img src='<?=$item['PICTURE_URL']?>' alt='<?=$item['NAME']?>' /></div>
                    <div class='wrap-caption'><span><?=$item['NAME']?></span></div>
                </label>
            </li>
        <?endforeach?>
        </ul>
        <p><a href="https://safe-pay.ru/?page_id=4720"  target="_blank"><?=Loc::getMessage('SAFEPAY_BLOCKCHAIN_QUESTION')?></a></p>
        <p><small><?=Loc::getMessage('SAFEPAY_BLOCKCHAIN_TEXT')?></small></p>
        <?if($params["NEW_WINDOW"] != "Y"):?>
            <input type="submit" value="<?=Loc::getMessage('SAFEPAY_BLOCKCHAIN_BTN_PAY')?>" />
        <?endif?>
    </form>
    <style>
        .wrap-select-bank{
            margin: 0;
            padding: 0;
        }
        .wrap-select-bank li{
            list-style: none;
            max-width:170px;
            display: inline;
        }
        .wrap-select-bank li label{
            margin: 5px;
            padding: 5px;
            display: inline-block;
            cursor: pointer;
        }
        .wrap-select-bank li label div.wrap-caption{
            max-width:100%;
            text-align: center;
            margin-top: 10px;
        }
        .wrap-select-bank li label div.wrap-img{
            width: 100px;
            height: 50px;
            text-align: center;
            position: relative;
        }
        .wrap-select-bank li label div.wrap-img img{
            max-width: 100px;
            max-height: 50px;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }
        .wrap-select-bank li input[type='radio']{
            display: none;
        }
        .wrap-select-bank li input[type='radio']:checked + label{
            border: 2px solid #16007b;
            margin: 3px;
        }
    </style>
    <?if($params["NEW_WINDOW"] == "Y"):?>
        <script>
            document.getElementById("payForm").submit();
        </script>
    <?endif?>
<?endif?>