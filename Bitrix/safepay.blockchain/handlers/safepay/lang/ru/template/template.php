<?php
$MESS["SAFEPAY_BLOCKCHAIN_BTN_PAY"] = "Оплатить";
$MESS["SAFEPAY_BLOCKCHAIN_BTN_CHECK_PAY"] = "Проверить оплату";
$MESS["SAFEPAY_BLOCKCHAIN_TEXT_SELECTED_PAY"] = "Пожалуйста, выберите банк, через который вы хотите оплатить заказ:";
$MESS["SAFEPAY_BLOCKCHAIN_QUESTION"] = "Не нашли свой банк ?";
$MESS["SAFEPAY_BLOCKCHAIN_TEXT"] = "Safe Pay - выставление электронного счёта в личный кабинет интернет-банка.<br/>Обращаем внимание, вы сможете воспользоваться этим способом оплаты, если является клиентом одного из перечисленных банков на сайте <a href=\"https://safe-pay.ru/#banks\" target=\"_blank\" >safe-pay.ru</a>.<br/> Нажимая кнопку \"Оформить заказ\" я подтверждаю, что ознакомлен и принял правила использование сервиса SAFE PAY опубликованные на сайте интернет-магазина.";