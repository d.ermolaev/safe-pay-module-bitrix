<?php
$MESS["SAFEPAY_BLOCKCHAIN_NAME"] = "���������� ������ (Safe Pay)";
$MESS["SAFEPAY_BLOCKCHAIN_DESC"] = 'Safe Pay - ����������� ������������ ����� � ������ ������� ��������-�����.<br /> �������� ��������, �� ������� ��������������� ���� �������� ������, ���� �������� �������� ������ �� ������������� ������ �� ����� <a href="https://safe-pay.ru/#banks" target="_blank" >safe-pay.ru</a>.';
//TABS
$MESS["SAFEPAY_BLOCKCHAIN_TAB_MODE"] = "����� ������";
$MESS["SAFEPAY_BLOCKCHAIN_TAB_BASE_OPTIONS"] = "������ ��������";
$MESS["SAFEPAY_BLOCKCHAIN_TAB_ORDER_INFO"] = "���������� �� ������";
$MESS["SAFEPAY_BLOCKCHAIN_TAB_MORE_PARAM"] = "�������������� ���������";

$MESS["SAFEPAY_BLOCKCHAIN_order_num"] = "����� ������";
$MESS["SAFEPAY_BLOCKCHAIN_order"] = "����� �������";
$MESS["SAFEPAY_BLOCKCHAIN_order_date"] = "���� ������";
$MESS["SAFEPAY_BLOCKCHAIN_sum"] = "����� �������";
$MESS["SAFEPAY_BLOCKCHAIN_sum_tax"] = "�������� �������";
$MESS["SAFEPAY_BLOCKCHAIN_curr_b"] = "������ �������";
$MESS["SAFEPAY_BLOCKCHAIN_curr"] = "��� ������ �  ISO 4217";
$MESS["SAFEPAY_BLOCKCHAIN_userPhone"] = "������� ����������";

$MESS["SAFEPAY_BLOCKCHAIN_expire"] = "���� �������� ������";
$MESS["SAFEPAY_BLOCKCHAIN_DESC_expire"] = "�� ��������� 24 ����";
$MESS["SAFEPAY_BLOCKCHAIN_title"] = "��������� ���������";
$MESS["SAFEPAY_BLOCKCHAIN_DESC_title"] = "";
$MESS["SAFEPAY_BLOCKCHAIN_description"] = "�������� ������";
$MESS["SAFEPAY_BLOCKCHAIN_DESC_description"] = "";
