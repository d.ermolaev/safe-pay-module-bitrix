<?php

use Bitrix\Main\Application;
use Bitrix\Sale\Internals\PaymentTable;
use SafePay\Blockchain\Procces;
use SafePay\Blockchain\Entitys\InvoiceTable;

require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php";

IncludeModuleLangFile(__FILE__);
$APPLICATION->SetTitle(GetMessage("SAFEPAY_BLOCKCHAIN_NAME_CAPTION"));
CModule::IncludeModule("sale");
CModule::IncludeModule("safepay.blockchain");

$POST_RIGHT = $APPLICATION->GetGroupRight("sale");
if ($POST_RIGHT == "D") {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$request = Application::getInstance()->getContext()->getRequest();

if ($request["refund"] == 'Y') {
    $procces = new Procces();
    $id = $request["ID"];
    $procces->refundPay($id);
} elseif($request["cancel"] == 'Y') {
    $procces = new Procces();
    $id = $request["ID"];
    $procces->canceledPay($id);
} elseif($request["update"] == 'list') {
    $procces = new Procces();
    $id = $request["ID"];
    $procces->resultPay($id);
}

$sTableID = "safe_pay_invoice";
$lAdmin = new CAdminList($sTableID);

$dbData = InvoiceTable::GetList(array(
    "order" => array("ID" => "desc"),
));

$dbData = new CAdminResult($dbData, $sTableID);
$dbData->NavStart();
$lAdmin->NavText($dbData->GetNavPrint(GetMessage("SAFEPAY_BLOCKCHAIN_NAV")));
$lAdmin->AddHeaders(array(
    array(
        "id" => "ID",
        "content" => "ID",
        "align" => "left",
        "default" => true,
    ),
    array(
        "id" => "ORDER_ID",
        "content" => GetMessage("SAFEPAY_BLOCKCHAIN_CAP_ORDER_ID"),
        "default" => true,
    ),
    array(
        "id" => "PAY_NUM_LINK",
        "content" => GetMessage("SAFEPAY_BLOCKCHAIN_CAP_PAY_NUM"),
        "default" => true,
    ),
    array(
        "id" => "STATUS_LANG",
        "content" => GetMessage("SAFEPAY_BLOCKCHAIN_CAP_STATUS"),
        "default" => true,
    ),
    array(
        "id" => "BANK_ID",
        "content" => GetMessage("SAFEPAY_BLOCKCHAIN_CAP_BANK_ID"),
        "default" => true,
    ),
    array(
        "id" => "DATE_UPDATED",
        "content" => GetMessage("SAFEPAY_BLOCKCHAIN_CAP_DATE_UPDATED"),
        "default" => true,
    ),
));

$contextMenu = array();
$contextMenu[] = array(
    'TEXT' => GetMessage('SAFEPAY_BLOCKCHAIN_ACTION_UPDATE'),
    'TITLE' => GetMessage('SAFEPAY_BLOCKCHAIN_ACTION_UPDATE'),
    'LINK' => 'safepay_blockchain_list.php?lang='.LANGUAGE_ID.'&update=list',
);
$lAdmin->AddAdminContextMenu($contextMenu);

while ($arRes = $dbData->NavNext(true, "f_")):
    if ($arRes['STATUS'] == 'active'){
        $arRes['STATUS_LANG'] = GetMessage('SAFEPAY_BLOCKCHAIN_STATUS_ACTIVE');
    } elseif ($arRes['STATUS'] == 'expired'){
        $arRes['STATUS_LANG'] = GetMessage('SAFEPAY_BLOCKCHAIN_STATUS_EXPIRED');
    } elseif ($arRes['STATUS'] == 'finish'){
        $arRes['STATUS_LANG'] = GetMessage('SAFEPAY_BLOCKCHAIN_STATUS_FINISH');
    } elseif ($arRes['STATUS'] == 'refund'){
        $arRes['STATUS_LANG'] = GetMessage('SAFEPAY_BLOCKCHAIN_STATUS_REFUND');
    } elseif ($arRes['STATUS'] == 'canceled'){
        $arRes['STATUS_LANG'] = GetMessage('SAFEPAY_BLOCKCHAIN_STATUS_CANCELED');
    }

    $row = &$lAdmin->AddRow($f_ID, $arRes);

    $arPayment = PaymentTable::getList(array(
        'filter' => array('ID' => $arRes['PAY_NUM']),
        'select' => array('ORDER_ID'),
    ))->fetch();
    $order_id = $arPayment['ORDER_ID'];

    $row->AddField("ORDER_ID", "<a href=\"sale_order_edit.php?ID=".$order_id."&lang=".$lang.GetFilterParams("filter_")."\">".$order_id."</a>");
    $row->AddViewField("PAY_NUM_LINK", '<a href="/bitrix/admin/sale_order_payment_edit.php?order_id='.$order_id.'&payment_id=' . $arRes['PAY_NUM'] . '">' . $arRes['PAY_NUM'] . '</a>');

    $arActions = array();
    if ($arRes['STATUS'] == 'finish') {
        $arActions[] = array(
            "ICON" => "delete",
            "DEFAULT" => false,
            "TEXT" => GetMessage("SAFEPAY_BLOCKCHAIN_ACTION_REFUND"),
            "ACTION" => $lAdmin->ActionRedirect("?ID=" . $f_ID . "&refund=Y"),
        );
    } elseif ($arRes['STATUS'] == 'active') {
        $arActions[] = array(
            "ICON" => "delete",
            "DEFAULT" => false,
            "TEXT" => GetMessage("SAFEPAY_BLOCKCHAIN_ACTION_CANCEL"),
            "ACTION" => $lAdmin->ActionRedirect("?ID=" . $f_ID . "&cancel=Y"),
        );
    }

    $row->AddActions($arActions);
endwhile;

$lAdmin->AddFooter(array(array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $dbData->SelectedRowsCount())));
$lAdmin->CheckListMode();
require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php";
$lAdmin->DisplayList();
require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php";
