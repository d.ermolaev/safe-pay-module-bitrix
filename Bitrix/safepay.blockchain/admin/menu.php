<?
IncludeModuleLangFile(__FILE__); 

if ($APPLICATION->GetGroupRight("sale")>"D") {
   
   $aMenu = array(
        "parent_menu" => "global_menu_store",
        "sort"        => 800,
        "url"         => "safepay_blockchain_list.php?lang=".LANGUAGE_ID,
        "text"        => GetMessage("SAFEPAY_BLOCKCHAIN_NAME_MENU"),
        "title"       =>  GetMessage("SAFEPAY_BLOCKCHAIN_NAME_MENU_TITLE"),
        "icon"        => "sys_menu_icon",
        "page_icon"   => "sys_menu_icon",
        "items_id"    => "menu_safepay_blockchain",
        "items"       => array(),
    );
   
   return $aMenu;
}
return false;
